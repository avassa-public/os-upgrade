#!/bin/sh

generate_build_job() {
    cat <<EOF
stages:
  - build
  - test

variables:
  SUBTENANT: os-upgrade-test-tenant
  SUPCTL_PROFILE_DIR: \$CI_PROJECT_DIR/test/.supctl

build:
  stage: build
  tags:
    - dm-env
  cache:
    - key: base-images
      paths:
        - test/base-images
  artifacts:
    access: developer
    expire_in: 1 day
    when: on_success
    paths:
      - \$SUPCTL_PROFILE_DIR
      - test/deps
  script:
    - cd test
    - make deps
    - mkdir -p base-images && ln -s ../../base-images deps/test-env/base
    - export PATH="\$PATH:\$(pwd)/deps/lux/bin:\$(pwd)/deps"
    - >
      supctl --host "api.\$AVASSA_ENV" --port 443
      do login "\$USERNAME" --password "\$PASSWORD" > /dev/null
    - > ## verify login
      supctl do token-info | grep -Fq "tenant: \$TENANT"
    - supctl delete tenants "\$SUBTENANT" && sleep 15 || true
    - |
      supctl create tenants <<EOF
      name: \$SUBTENANT
      kind: site-provider
      policies:
        - root
        - site-provider-tenant
      EOF
    - export SUBTENANT_USERNAME=\$SUBTENANT@avassa.io
    - export SUBTENANT_PASSWORD=\$(head -c 32 /dev/random | base64)
    - >
      supctl do strongbox system setup-tenant "\$SUBTENANT"
      "\$SUBTENANT_USERNAME" --admin-password "\$SUBTENANT_PASSWORD" >/dev/null
    - >
      supctl do login "\$SUBTENANT_USERNAME"
      --password "\$SUBTENANT_PASSWORD" > /dev/null
    - > ## verify login
      supctl do token-info | grep -Fq "tenant: \$SUBTENANT"
    - > ## save password for debug
      echo "\$SUBTENANT_PASSWORD" > "\$SUPCTL_PROFILE_DIR"/password
    - >
      supctl create strongbox authentication approles < ../yaml/approle.yaml
    - >
      echo \$CI_REGISTRY_PASSWORD |
      docker login -u \$CI_REGISTRY_USER --password-stdin \$CI_REGISTRY
    - >
      echo "\$SUBTENANT_PASSWORD" |
      docker login -u \$SUBTENANT_USERNAME --password-stdin
      registry.\$AVASSA_ENV
    - make VSN=$CI_COMMIT_SHA build

.lux-test:
  stage: test
  needs:
    - build
  tags:
    - dm-env
  cache:
    - key: base-images
      paths:
        - test/base-images
  artifacts:
    expire_in: 1 day
    when: on_failure
    paths:
      - test/lux_logs
  variables:
    LIBVIRT_DEFAULT_URI: qemu:///system
    LUX_SYSTEM_FLAGS: --log_dir \$CI_PROJECT_DIR/test/lux_logs --multiplier=2000
  before_script:
    - seq -f 'h%02.0f' 0 9 | xargs -I{} virsh destroy {} >/dev/null 2>&1 || true
    - cd test
    - export PATH="\$PATH:\$(pwd)/deps/lux/bin:\$(pwd)/deps"
  after_script:
    - ./test/deps/test-env/test-env.sh destroy
EOF
}

generate_lux_job() {
    path=${1?:}
    dirpath=$(dirname "${path}")
    name=$(basename "${path%.lux}")
    cat <<EOF

# ${path}
test-$name:
  extends: .lux-test
  script:
    - make LUX_FILES=${path} cache
    - cd ${dirpath}
    - lux ${name}.lux
EOF
}

generate_build_job
cd "$(dirname $0)/../test"
for t in $(lux --mode list lux/); do
    generate_lux_job "$t"
done
