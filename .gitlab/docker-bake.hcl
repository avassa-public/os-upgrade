variable "VSN" { }
variable "CI_COMMIT_SHA" { }
variable "CI_REGISTRY_IMAGE" { }
group "ci" {
    targets = [ "build", "test" ]
}
target "build" {
    dockerfile = "Dockerfile.build"
    platforms = [ "linux/amd64", "linux/arm64" ]
    cache-from = [ "type=registry,ref=${CI_REGISTRY_IMAGE}/cache" ]
    cache-to = [ "type=registry,ref=${CI_REGISTRY_IMAGE}/cache" ]
    output = [ "type=registry" ]
    tags = [ "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}" ]
}
target "test" {
    dockerfile = "Dockerfile.build"
    target = "test"
    cache-from = [ "type=registry,ref=${CI_REGISTRY_IMAGE}/test-cache" ]
    cache-to = [ "type=registry,ref=${CI_REGISTRY_IMAGE}/test-cache" ]
    output = [ "type=registry" ]
    tags = [ "${CI_REGISTRY_IMAGE}/test:${CI_COMMIT_SHA}" ]
}

target "lux" {
    dockerfile = ".gitlab/Dockerfile.lux"
    output = [ "type=registry" ]
    tags = [ "registry.gitlab.com/avassa-public/os-upgrade/lux:latest" ]
}
