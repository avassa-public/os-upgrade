# OS Upgrade Worker
This repository contains the OS upgrade worker application implementing
integrations with a number of distributions listed below. This application is
intended to be used in combination with the Avassa platform's
[automated OS upgrade feature](https://docs.avassa.io/tutorials/os-upgrades).

## Currently supported distributions
* Debian/Ubuntu
* RedHat's RHEL for Edge
* Fedora CoreOS

## Overview
An OS upgrade worker for a Linux distribution integrates with the
distribution's automatic upgrade mechanism. When it receives a command from the
OS upgrade controller, it translates it into the distribution-specific actions.

The following commands are defined:
  - `prepare` command is intended to trigger a prepare for upgrade phase. This
    phase is not intended for any potentically destructive actions, and hence
    executed simultaneously on all workers in a site. An example of this phase
    is to download a newer version of the image.
  - `upgrade` command is intended to trigger an upgrade, e.g. replace the
    executables, libraries and other files with the newer versions and run the
    required upgrade processes. This is a potentially destructive phase which
    may bring the system into an unoperable state in case of an upgrade
    failure, hence it is executed one host at at time.
  - `reboot` command is optional (requested in a response to `upgrade` command).
    In this phase the worker may choose to reboot the machine, for example in
    order to activate the new version of the Linux kernel. However, from the
    controller's point of view this phase is similar in semantics to the
    `upgrade` phase and serves only as a checkpoint during the `upgrade` phase.

In addition to this, the OS upgrade worker implementation defines a set of keys
and values representing the current system version. The worker is responsible
for identifying these values and publishing them whenever this set changes. The
controller saves the latest set of values reported by the OS worker and makes
it available over the Avassa API, see [OS upgrade host state
](https://avassa-api.redoc.ly/tag/OS-upgrade-host-state).

See the [reference documentation on OS upgrades
](https://avassa-api.redoc.ly/tag/OS-upgrade-management) for a more complete
description of the protocol between the controller and the workers.

The following sections go into details of how the worker is implemented for
different distributions.

## Debian/Ubuntu implementation
The automatic upgrades for Debian/Ubuntu is implemented by integrating with
the `unattended-upgrades` package. A `systemd` socket is mounted into the
worker's container and the `pystemd` Python library is used to control the
systemd services installed by the `unattended-upgrades` package.

At the worker initialization time the application modifies the
`unattended-upgrades` configuration to disable periodic upgrades and disables
the `apt-daily` and `apt-daily-upgrade` systemd services.

In the `prepare` phase the `apt-daily` service is launched, which updates the
index and downloads the new versions of the packages.

In the `upgrade` phase the `apt-daily-upgrade` service is launched, which
upgrades the packages to the new versions downloaded in the `prepare` phase.
After this existence of the `/var/run/reboot-required` file is checked to
identify the need for the optional `reboot phase`.

In the `reboot` phase the systemd `reboot.target` is launched. A successful
reboot is reported the next time the worker starts after the reboot.

The key-value list identifying the system version is the set of all apt
package names together with their versions. The implementation obtains the
list of currently installed packages by running the `dpkg-query --show` command
using a transient systemd unit.

## RedHat's RHEL for Edge
RHEL for Edge utilizes A/B system updates. This means there are no individual
package upgrades as in the Debian/Ubuntu case, but instead the image containing
a set of packages is upgraded as a whole. This implies completely replacing
parts of the filesystem containing the system files.

To allow access to the Red Hat Console, you must provide a client ID and secret.
These are configured in a vault called `redhat-console` with a secret `client`. See
[secrets.rhel.yaml.in](yaml/secrets.rhel.yaml.in).

In the `prepare` phase the list of available image versions is fetched and an
upgrade candidate is identified.

If there is an upgrade candidate, then in the `upgrade` phase a request to
upgrade to this new version of image is sent. Each upgrade is associated with
a reboot on RHEL for Edge, so the worker proceeds with a `reboot` phase
unconditionally in this case. If there is no upgrade candidate, then the
`upgrade` phase is a no-op and no `reboot` phase is requested.

In the `reboot` phase the worker monitors the progress of the upgrade request
and reports if there is an error, otherwise does nothing waiting for the system
to be rebooted. After the reboot it checks the status of the latest upgrade
again and returns the response accordingly.

The system version is identified by 3 keys: `image`, `version` and `url`. The
`image` is the name of the image in the RedHat console. The `version` is the
currently installed revision of this image. The `url` is provided for
convenience and contains a URL to the RedHat console describing the specific
image revision in detail, including the set of installed packages and their
versions.

## Fedora CoreOS
Similarly to RHEL for Edge, Fedora CoreOS utilizes A/B system updates. Parts of
the filesystem are completely replaced and the old version is kept for rollback.

This worker utilizes the fleet lock functionality to orchestrate Fedora CoreOS
upgrades. The system must be configured to request a fleet lock from
`http://127.0.0.1:8000`, while the worker runs in the host networking mode and
starts a web server listening on this port at initialization time. Unless the
worker is in the `reboot` phase, all lock requests are declined.

This can be achieved by creating `/etc/zincati/config.d/51-fleet-update.toml`, 
preferably through ignition.
```toml
[updates]
strategy = "fleet_lock"

[updates.fleet_lock]
base_url = "http://127.0.0.1:8000/fleet_lock/"
```

The `prepare` phase is a no-op.

At the start of the `upgrade` phase the worker identifies whether there has been
any upgrade requests since worker initialization or since the last upgrade. If
there has been an upgrade request, the worker assumes it should await a re-try
and proceeds to the `reboot` phase.

In the `reboot` phase the worker waits for a lock request and when it arrives
the fleet lock is granted. The OS then proceeds with the upgrade. CoreOS
upgrade always implies a reboot. After the upgrade the worker waits for the
lock to be released, then reports the `reboot` to be successful.

There is not much visibility into this process, so the error handling is based
solely on timeouts. I.e., if the upgrade failed, it is assumed that the lock
release is not requested, or the worker is not restarted, in which case the
site upgrade is aborted by timeout.
