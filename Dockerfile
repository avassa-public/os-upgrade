arg IMAGE=registry.gitlab.com/avassa-public/os-upgrade
arg VSN=latest

from ${IMAGE}:${VSN}

arg invalidate_approle_id

run --mount=type=secret,id=approle,target=/secret cp /secret /app-role-id
