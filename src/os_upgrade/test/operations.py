import logging
import os
import platform

from .. import systemd

logger = logging.getLogger(__name__)

class TestState:
    def __init__(self, host, client_wrapper):
        self.version = client_wrapper.read_versions(host)
        self._publish = client_wrapper.publish_versions
        if self.version is None:
            self.update_version(0)

    def update_version(self, version):
        self.version = {'version': version, 'linux': platform.uname().release}
        return self._publish(self.version)

async def init(host, client_wrapper):
    logger.debug('start init')
    state = TestState(host, client_wrapper)
    logger.debug('init done')
    return state

async def prepare(state):
    logger.info('prepare for upgrade')
    result = 'done'
    if 'FAIL_PREPARE' in os.environ and os.environ['FAIL_PREPARE'].find(os.environ['HOST']) != -1:
        result = 'not completed'
    logger.info('prepare for upgrade: %s', result)
    return result, state

async def upgrade(state):
    logger.info('start upgrade')
    vsn = state.version['version']
    if 'FAIL_UPGRADE' in os.environ and os.environ['FAIL_UPGRADE'].find(os.environ['HOST']) != -1:
        result = 'not completed'
    else:
        result = 'done'
        if 'REBOOT_REQUIRED' in os.environ and os.environ['REBOOT_REQUIRED'] == 'yes':
            result = 'reboot-required'
            logger.info('upgrade from version %d: %s', vsn, result)
        else:
            state.update_version(vsn + 1)
            logger.info('upgrade from version %d to %d: %s', vsn, state.version['version'], result)
    return result, state

async def reboot(state):
    logger.info('start reboot')
    systemd.reboot()
    return 'pending', state

async def after_reboot(state):
    logger.info('reboot completed')
    state.update_version(state.version['version'] + 1)
    logger.info('upgrade to version %d: done', state.version['version'])
    return 'done', state
