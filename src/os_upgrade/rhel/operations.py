import asyncio
import datetime
import logging

import aiohttp

logger = logging.getLogger(__name__)

class ApiException(Exception):
    pass

class RHELState:
    def __init__(self, host, client_wrapper):
        self.version = client_wrapper.read_versions(host)
        self._publish = client_wrapper.publish_versions
        self._token = None
        self._token_exp = datetime.datetime.now()
        self._token_type = None
        self.api_id = None
        self.sub_mgr_id = None
        self.upgrade_candidate = None
        self.upgrade_id = None

    @classmethod
    async def create(cls, host, client_wrapper):
        obj = cls(host, client_wrapper)
        async with aiohttp.ClientSession() as session:
            obj.api_id, obj.sub_mgr_id = await obj._get_api_id(session)
            logger.info('API ID: %s', obj.api_id)
            logger.info('Subscription manager ID: %s', obj.sub_mgr_id)
            status = await obj.get_status(session)
            if status is not None:
                obj.update_version(status)
                if "UpdateTransactions" in status and len(status["UpdateTransactions"]) > 0:
                    obj.upgrade_id = status["UpdateTransactions"][0]["ID"]
            else:
                ## die and be restarted
                raise ApiException("could not fetch device status")
        return obj

    async def auth(self, session):
        if self._token is None or self._token_exp < datetime.datetime.now():
            await self._renew_token(session)
        return f'{self._token_type} {self._token}'

    async def _renew_token(self, session):
        with open('/auth/id', 'r', encoding='latin1') as f:
            client_id = f.readline().rstrip('\n')
        with open('/auth/secret', 'r', encoding='latin1') as f:
            client_secret = f.readline().rstrip('\n')
        url = 'https://sso.redhat.com/auth/realms/redhat-external'\
              '/protocol/openid-connect/token'
        data = {'client_id': client_id,
                'client_secret': client_secret,
                'grant_type': 'client_credentials',
                'scope': 'api.console'
               }
        self._token = None
        self._token_exp = datetime.datetime.now()
        async with session.post(url, data=data) as response:
            if response.status != 200:
                raise ApiException('could not generate auth token: '\
                                  f'{response.status} {response.reason}')
            res = await response.json(encoding='utf-8')
        self._token = res['access_token']
        self._token_exp += datetime.timedelta(seconds=res['expires_in']-10)
        self._token_type = res['token_type']

    async def _get_api_id(self, session):
        with open('/insights-id', 'r', encoding='latin1') as f:
            insights_id = f.readline().rstrip('\n')
        url = 'https://console.redhat.com/api/inventory/v1/hosts'
        params = {'filter[system_profile][host_type]': 'edge',
                  'insights_id': insights_id}
        headers = {'Authorization': await self.auth(session)}
        async with session.get(url, params=params, headers=headers) as response:
            if response.status != 200:
                raise ApiException('could not retrieve API ID: '\
                                  f'{response.status} {response.reason}')
            res = await response.json(encoding='utf-8')
            logger.debug('GET %s (%s):\n%s', url, params, res)
        if res["total"] < 1:
            raise ApiException(f'host insights_id={insights_id} not found'\
                               ' in /api/inventory/v1/hosts')
        if res["total"] > 1:
            raise ApiException(f'multiple matches for {insights_id} in'
                               f' /api/inventory/v1/hosts: {res["total"]}')
        res = res["results"][0]
        return res["id"], res["subscription_manager_id"]

    async def get_status(self, session):
        url = f'https://console.redhat.com/api/edge/v1/devices/{self.api_id}'
        headers = {'Authorization': await self.auth(session)}
        async with session.get(url, headers=headers) as response:
            if response.status != 200:
                raise ApiException(\
                        'could not retrieve device {self.api_id} status: '\
                       f'{response.status} {response.reason}')
            res = await response.json(encoding='utf-8')
            # logger.debug('GET %s:\n%s', url, res)
            return res

    def update_version(self, status):
        current_image = status["ImageInfo"]["Image"]
        image_name = current_image["Name"]
        version = current_image["Version"]
        image_set_id = current_image["ImageSetID"]
        image_id = current_image["ID"]
        url = 'https://console.redhat.com/edge/manage-images/'\
             f'{image_set_id}/versions/{image_id}/details'
        if self.version is None or 'url' not in self.version or self.version['url'] != url:
            logger.info("update image version: %s:%d", image_name, version)
            self.version = {'image': image_name,
                            'version': version,
                            'url': url}
            return self._publish(self.version)
        return True

    async def update_version_from_api(self, session):
        status = await self.get_status(session)
        if status is not None:
            self.update_version(status)

async def init(host, client_wrapper):
    logger.debug('start init')
    state = await RHELState.create(host, client_wrapper)
    logger.debug('init done')
    return state

async def prepare(state):
    logger.info('prepare for upgrade')
    try:
        result, state = await _fetch_update_candidate(state)
        logger.info('prepare for upgrade: %s', result)
    except ApiException as e:
        logger.error('exception in prepare: %s', e)
        result = str(e)
    return result, state

async def _fetch_update_candidate(state):
    url = 'https://console.redhat.com/api/edge/v1/updates/device/'\
         f'{state.api_id}/updates'
    async with aiohttp.ClientSession() as session:
        headers = {'Authorization': await state.auth(session)}
        async with session.get(url, headers=headers) as response:
            if response.status != 200:
                raise ApiException(\
                        'could not retrieve device {self.api_id} updates: '\
                       f'{response.status} {response.reason}')
            res = await response.json(encoding='utf-8')
            # logger.debug('GET %s:\n%s', url, res)
    state.upgrade_info = None
    state.upgrade_candidate = None
    if res is not None and len(res) > 0:
        state.upgrade_info = res[0]
        state.upgrade_candidate = state.upgrade_info["Image"]["CommitID"]
        logger.info("upgrade candidate image ID: %d", state.upgrade_candidate)
    return 'done', state

async def upgrade(state):
    logger.info('start upgrade')
    try:
        if state.upgrade_candidate is None:
            logger.info("no upgrade candidate, skipping")
            result = 'done'
        else:
            state = await _invoke_upgrade_operation(state)
            result = 'reboot-required'
    except ApiException as e:
        logger.error('exception in prepare: %s', e)
        result = str(e)
    logger.info('upgrade: %s', result)
    return result, state

async def _invoke_upgrade_operation(state):
    url = 'https://console.redhat.com/api/edge/v1/updates'
    data = {'CommitID': state.upgrade_candidate,
            'DevicesUUID': [state.api_id]
           }
    async with aiohttp.ClientSession() as session:
        headers = {'Authorization': await state.auth(session)}
        async with session.post(url, headers=headers, json=data) as response:
            if response.status != 200:
                return f'upgrade failed: {response.status} {response.reason}'
            ## NOTE: the server sends incorrect Content-type, ignore it
            res = await response.json(encoding='utf-8', content_type=None)
    logger.debug('POST %s (%s):\n%s', url, data, res)
    state.upgrade_id = res[0]["ID"]
    logger.info('upgrade status %s id %d', res[0]["Status"], state.upgrade_id)
    return state

async def reboot(state):
    async with aiohttp.ClientSession() as session:
        result = await _await_upgrade(session, state.upgrade_id, state)
    if result == 'done':
        result = 'unexpected upgrade success with no reboot'
        ## update the version after upgrade just in case
        await state.update_version_from_api(session)
    return result, state

async def _await_upgrade(session, upgrade_id, state):
    while True:
        url = f'https://console.redhat.com/api/edge/v1/updates/{upgrade_id}'
        headers = {'Authorization': await state.auth(session)}
        async with session.get(url, headers=headers) as response:
            if response.status != 200:
                return 'waiting for upgrade to complete failed: '\
                      f'{response.status} {response.reason}'
            ## NOTE: the server sends incorrect Content-type, ignore it
            res = await response.json(encoding='utf-8', content_type=None)
        logger.debug('GET %s:\n%s', url, res)
        if res['Status'] == 'BUILDING':
            await asyncio.sleep(30)
        elif res['Status'] == 'SUCCESS':
            return 'done'
        else:
            status = res['Status']
            dispatch_records = res['DispatchRecords']
            if len(dispatch_records) > 0:
                dispatch_record = dispatch_records[0]
                if dispatch_record['Status'] != status:
                    status += f': {dispatch_record["Status"]}'
                if 'Reason' in dispatch_record:
                    status += f': {dispatch_record["Reason"]}'
            return status

async def after_reboot(state):
    ## state.upgrade_id should be initialized in init when we fetch our status
    if state.upgrade_id is None:
        return 'upgrade not found', state
    async with aiohttp.ClientSession() as session:
        result = await _await_upgrade(session, state.upgrade_id, state)
        await state.update_version_from_api(session)
    return result, state
