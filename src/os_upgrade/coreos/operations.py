import json
import logging
import asyncio

from aiohttp import web

AVASSA_API = 'https://127.0.0.1:4646'

logger = logging.getLogger(__name__)


class CoreOSState:
    def __init__(self):
        self.upgrade_allowed = False
        self.os_reported_reboot = asyncio.get_running_loop().create_future()
        self.upgrade_requested = False
        self.web_site = None

    @classmethod
    async def create(cls, _host, _client_wrapper):
        obj = cls()
        web_app = web.Application()
        web_app.add_routes([
            web.post('/fleet_lock/v1/steady-state', obj.fl_steady_state),
            web.post('/fleet_lock/v1/pre-reboot', obj.fl_pre_reboot)
            ])
        runner = web.AppRunner(web_app)
        await runner.setup()
        obj.web_site = web.TCPSite(runner, 'localhost', 8000)
        await obj.web_site.start()
        return obj

    async def fl_steady_state(self, _request):
        if self.os_reported_reboot:
            # Signal we are rebooted
            if not self.os_reported_reboot.done():
                self.os_reported_reboot.set_result(None)
        else:
            resp = {
                    "kind": "error",
                    "value": "Can't release lock at the moment"
                    }
            raise web.HTTPNotAcceptable(text=json.dumps(resp))
        return web.Response()

    async def fl_pre_reboot(self, _request):
        self.upgrade_requested = True
        if not self.upgrade_allowed:
            logger.info("Declining upgrade")
            resp = {
                    "kind": "not_allowed",
                    "value": "Upgrades not allowed yet"
                    }
            raise web.HTTPNotAcceptable(text=json.dumps(resp))
        logger.info("Allowing upgrade")
        return web.Response()


async def init(host, client_wrapper):
    logger.debug('start init')
    state = await CoreOSState.create(host, client_wrapper)
    logger.debug('init done')
    return state


# On CoreOS there is no prepare phase we can control
async def prepare(state):
    logger.info('prepare for upgrade')
    return 'done', state


async def upgrade(state):
    logger.info('start upgrade')
    if state.upgrade_requested:
        # Indicate to the web server that upgrades are allowed
        # Zincati will ask again
        state.upgrade_allowed = True
        logger.info("Upgrade was requested, go to reboot")
        # If an upgrade is requested, we will be rebooted, sooner or later...
        result = 'reboot-required'
    else:
        # If no upgrade is requested, just return here
        logger.info("No upgrade was requested")
        result = 'done'
    return result, state


async def reboot(state):
    logger.info('reboot')
    result = 'pending'
    # Make sure fl_steady_state returns error until we recreate this
    # after reboot (in the init function)
    state.os_reported_reboot = None
    return result, state


async def after_reboot(state):
    logger.info('after_reboot - wait for os')
    # Create semaphore, when CoreOS calls steady-state (after reboot)
    # this will be released
    await state.os_reported_reboot
    logger.info('after_reboot - done')
    return 'done', state
