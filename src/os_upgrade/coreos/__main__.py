import asyncio
import logging
from . import operations
from .. import worker

logger = logging.getLogger(__package__)
asyncio.run(worker.main(logger, operations))
