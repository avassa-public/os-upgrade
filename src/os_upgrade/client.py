import asyncio
import json
import os
import urllib
import urllib.parse

import avassa_client
import avassa_client.volga

RECONNECT_TIMEOUT = 10


class ClientWrapper:
    CONTROL_TOPIC = 'os-upgrade:control'
    VERSIONS_TOPIC = 'os-upgrade:versions'

    def __init__(self, operations):
        self.sess = None
        self.base_url = getattr(operations, 'AVASSA_API', 'https://api:4646')
        self._login()

    def _role_id(self):
        try:
            with open('/app-role-id', 'r', encoding='latin1') as roleid_f:
                return roleid_f.readline().rstrip('\n')
        except FileNotFoundError:
            return None

    def _login(self):
        secret_id = os.environ['APPROLE_SECRET_ID']
        self.sess = avassa_client.approle_login(
            self.base_url, self._role_id(), secret_id, server_hostname='api')

    def url(self, path):
        if not path.startswith('/'):
            path = '/' + path
        return self.base_url + path

    # the following methods define REST-based volga operations
    def consume_last_message(self, topic_name):
        path = f'/v1/state/volga/topics/{urllib.parse.quote(topic_name)}/consume'
        payload = {
            'position-sequence-number': -1,
            'payload-only': True
        }
        headers = {'Accept': 'application/jsonlines'}
        try:
            _, _, _, body = avassa_client.post_request(
                self.sess, path, payload, headers=headers)
            if body.strip() == '':
                return None
            return json.loads(body)
        except urllib.error.URLError:
            return None

    def produce_once(self, topic_name, message):
        path = f'/v1/state/volga/topics/{topic_name}/produce'
        payload = {'payload': message}
        try:
            avassa_client.post_request(self.sess, self.url(path), payload)
            return True
        except urllib.error.URLError:
            return False

    def publish_versions(self, versions):
        return self.produce_once(self.VERSIONS_TOPIC, versions)

    def read_versions(self, host):
        path = f'/v1/state/os-upgrade/hosts/{host}'
        try:
            code, _, _, body = avassa_client.get_request(
                self.sess, self.url(path))
            if code != 200:
                return None
            return json.loads(body)['versions']
        except urllib.error.URLError:
            return None

    # the following methods define websocket-based volga operations
    def get_control_topic_consumer(self, name, position, end_marker=None):
        create_opts = avassa_client.volga.CreateOptions.wait()
        topic = avassa_client.volga.Topic.local(self.CONTROL_TOPIC)
        return avassa_client.volga.Consumer(session=self.sess,
                                            consumer_name=name,
                                            mode='exclusive',
                                            position=position,
                                            topic=topic,
                                            on_no_exists=create_opts,
                                            end_marker=end_marker)


async def login(operations):
    return await asyncio.to_thread(ClientWrapper, operations)
