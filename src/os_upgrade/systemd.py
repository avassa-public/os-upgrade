import asyncio
import datetime
import fcntl
import io
import os
import uuid

import pystemd
import pystemd.dbuslib
import pystemd.run
import pystemd.utils

from . import APPNAME

class RunError(Exception):
    pass

class UnitStateError(Exception):
    def __init__(self, name, state_type, state):
        self.name = name
        self.state_type = state_type
        self.state = state

    def __str__(self):
        return f'{self.name} bad {self.state_type} state: {self.state.decode()}'

class Job:
    def __init__(self):
        self.job = None
        self.service_name = None
        self.start_time = datetime.datetime.now()
        self.result = None

class ServiceProperties:
    def __init__(self):
        self.main_pid = 0
        self.exit_code = None

def get_unit(unit_name):
    unit = pystemd.SDUnit(unit_name, _autoload=True)
    load_state = unit.Unit.LoadState
    if load_state != b'loaded':
        raise UnitStateError(unit_name, 'load', load_state)
    return unit

async def sync_stop(unit_name):
    job = Job()
    future, start_subscriber = _create_job_removed_subscriber(job)
    # pylint: disable=no-member
    job.job = pystemd.SDManager(_autoload=True).StopUnit(unit_name, b'replace')
    start_subscriber()
    job = await future
    return job.result

def init_start(service_name):
    service = get_unit(service_name)
    active_state = service.Unit.ActiveState
    if active_state != b'inactive':
        raise UnitStateError(service_name, 'active', active_state)
    state = Job()
    future, start_subscriber = _create_job_removed_subscriber(state)
    state.service_name = service_name
    state.job = service.Unit.Start(b'replace')
    start_subscriber()
    return future

def disable(unit_name):
    # pylint: disable=no-member
    return pystemd.SDManager(_autoload=True).DisableUnitFilesWithFlags([unit_name], 0)

async def get_journal_extract(service_name, start_time=None):
    if start_time is None:
        start_time = datetime.datetime.now() - datetime.timedelta(hours = 1)
    start_time = start_time.replace(microsecond = 0)
    start_time_tz = start_time.astimezone().tzinfo
    cmd = [b'/usr/bin/journalctl', b'-q',
           b'-S', str(start_time).encode(),
           b'-u', service_name.encode()]
    return await run_collect_output(cmd, env={'TZ':str(start_time_tz)})

async def run_collect_output(cmd, env=None):
    loop = asyncio.get_running_loop()
    read_fd, write_fd = os.pipe()
    ## we only need the read end of the pipe to be non-blocking
    fcntl.fcntl(read_fd, fcntl.F_SETFL, os.O_NONBLOCK)
    try:
        try:
            buf = io.BytesIO()
            read_done = loop.create_future()
            def copy_from_pipe():
                chunk = os.read(read_fd, 8192)
                if len(chunk) == 0:
                    loop.remove_reader(read_fd)
                    if not read_done.cancelled():
                        read_done.set_result(buf.getvalue().decode())
                else:
                    buf.write(chunk)
            loop.add_reader(read_fd, copy_from_pipe)
            service_properties = await _systemd_run(cmd = cmd, env = env, stdout = write_fd)
        finally:
            os.close(write_fd)
        if service_properties.exit_code != 0:
            read_done.cancel()
            try:
                await read_done
            except asyncio.CancelledError:
                pass
            raise RunError(service_properties.exit_code)
        return await read_done
    finally:
        loop.remove_reader(read_fd)
        os.close(read_fd)

async def write_to_host_file(filepath, buf):
    loop = asyncio.get_running_loop()
    read_fd, write_fd = os.pipe()
    write_closed = False
    ## we only need the write end of the pipe to be non-blocking
    ## tee expects blocking input
    fcntl.fcntl(write_fd, fcntl.F_SETFL, os.O_NONBLOCK)
    try:
        write_done = loop.create_future()
        def write_to_pipe():
            nonlocal buf, write_closed
            nbytes = os.write(write_fd, buf)
            buf = buf[nbytes:]
            if len(buf) == 0:
                loop.remove_writer(write_fd)
                os.close(write_fd)
                write_closed = True
                if not write_done.cancelled():
                    write_done.set_result(None)
        loop.add_writer(write_fd, write_to_pipe)
        cmd = [b'/bin/tee', filepath.encode()]
        service_properties = await _systemd_run(cmd = cmd, stdin = read_fd)
        if service_properties.exit_code != 0:
            loop.remove_writer(write_fd)
            write_done.cancel()
            try:
                await write_done
            except asyncio.CancelledError:
                pass
            raise RunError(service_properties.exit_code)
        await write_done
    finally:
        loop.remove_writer(write_fd)
        os.close(read_fd)
        if not write_closed:
            os.close(write_fd)

def reboot():
    get_unit('reboot.target').Unit.Start(b'replace-irreversibly')

async def _systemd_run(**kwargs):
    name = f'{APPNAME}-{uuid.uuid4().hex}.service'
    objpath = _unit_path_encode(name)
    state = ServiceProperties()
    future, start_subscriber = _create_properties_changed_subscriber(objpath, state)
    # pylint: disable=not-callable
    pystemd.run(name = name, service_type = b'oneshot', collect = True, **kwargs)
    start_subscriber()
    return await future

def _create_subscriber():
    loop = asyncio.get_running_loop()
    future = loop.create_future()
    # pylint: disable=c-extension-no-member
    dbus = pystemd.dbuslib.DBus()
    dbus.open()
    fd = dbus.get_fd()
    def start_subscriber():
        loop.add_reader(fd, dbus.process)
    def subscriber_done(_future):
        loop.remove_reader(fd)
        dbus.close()
    future.add_done_callback(subscriber_done)
    return future, start_subscriber, dbus.match_signal

def _create_job_removed_subscriber(state):
    """
    Subscribe to JobRemoved signal.
    """
    future, start_subscriber, match_signal = _create_subscriber()
    match_signal(None, None,
            b'org.freedesktop.systemd1.Manager',
            b'JobRemoved',
            _job_removed_signal_cb,
            (future, state))
    return future, start_subscriber

def _job_removed_signal_cb(msg, error=None, userdata=None):
    # Signal parameters are as follows:
    # JobRemoved(u id, o job, s unit, s result);
    #https://www.freedesktop.org/software/systemd/man/latest/org.freedesktop.systemd1.html
    msg.process_reply(False)
    future, state = userdata
    _, job, _, result = msg.body
    if job == state.job:
        state.result = result.decode()
        future.set_result(state)

def _unit_path_encode(name):
    external_id = pystemd.utils.x2char_star(name)
    # pylint: disable=c-extension-no-member
    return pystemd.dbuslib.path_encode(b'/org/freedesktop/systemd1/unit', external_id)

def _create_properties_changed_subscriber(object_path, state):
    """
    Subscribe to PropertiesChanged signal.
    """
    future, start_subscriber, match_signal = _create_subscriber()
    match_signal(b'org.freedesktop.systemd1', object_path,
            b'org.freedesktop.DBus.Properties',
            b'PropertiesChanged',
            _properties_changed_signal_cb,
            (future, state))
    return future, start_subscriber

def _properties_changed_signal_cb(msg, error=None, userdata=None):
    # Signal parameters are as follows:
    # org.freedesktop.DBus.Properties.PropertiesChanged
    #    (STRING interface_name,
    #     ARRAY of DICT_ENTRY<STRING,VARIANT> changed_properties,
    #     ARRAY<STRING> invalidated_properties);
    # https://dbus.freedesktop.org/doc/dbus-specification.html
    msg.process_reply(False)
    future, state = userdata
    interface_name, changed_properties, _invalidated_properties = msg.body
    if interface_name == b'org.freedesktop.systemd1.Service':
        new_main_pid = changed_properties.get(b'MainPID', 0)
        exec_main_pid = changed_properties.get(b'ExecMainPID', 0)
        if state.main_pid == 0 and new_main_pid != 0 and new_main_pid == exec_main_pid:
            ## main process started
            state.main_pid = new_main_pid
        elif state.main_pid != 0 and new_main_pid == 0 and b'ExecMainStatus' in changed_properties:
            ## main process finished
            state.exit_code = changed_properties[b'ExecMainStatus']
            future.set_result(state)
