import logging
import os

from .. import systemd

logger = logging.getLogger(__name__)

class Debian:
    prepare_service_name = 'apt-daily.service'
    upgrade_service_name = 'apt-daily-upgrade.service'
    automatic_prepare_timer = 'apt-daily.timer'
    automatic_upgrade_timer = 'apt-daily-upgrade.timer'
    update_timestamp_files = ['/var/lib/apt/periodic/update-stamp',
                              '/var/lib/apt/periodic/download-upgradeable-stamp']
    upgrade_timestamp_files = ['/var/lib/apt/periodic/upgrade-stamp']
    container_apt_config_dir = '/data/apt.conf.d'
    host_apt_config_dir = '/etc/apt/apt.conf.d'
    reboot_required_file = '/var/run/reboot-required'

    def __init__(self, host, client_wrapper):
        self.packages = client_wrapper.read_versions(host)
        self._publish = client_wrapper.publish_versions
        self.update_timestamps = [None for file in self.update_timestamp_files]
        self.upgrade_timestamps = [None for file in self.upgrade_timestamp_files]

    def update_packages(self, packages):
        self.packages = packages
        return self._publish(self.packages)

async def init(host, client_wrapper):
    logger.debug('start init')
    state = Debian(host, client_wrapper)
    _verify_service(state.upgrade_service_name)
    await _disable_unit(state.automatic_upgrade_timer)
    await _disable_unit(state.automatic_prepare_timer)
    for filename in os.listdir(state.container_apt_config_dir):
        container_filepath = os.path.join(state.container_apt_config_dir, filename)
        with open(container_filepath, 'rb') as conf_file:
            buf = conf_file.read()
        host_filepath = os.path.join(state.host_apt_config_dir, filename)
        await systemd.write_to_host_file(host_filepath, buf)
    ## init packages from the topic
    state = await _log_update_packages(state, 'package changes at init')
    logger.debug('init done')
    return state

async def prepare(state):
    logger.info('prepare for upgrade')
    await _save_timestamps(state.update_timestamps, state.update_timestamp_files)
    future = _start_prepare_service(state)
    result = await _wait_service_done_print_log(future)
    if result == 'done':
        if not await _check_timestamps(state.update_timestamps, state.update_timestamp_files):
            result = 'not completed'
    logger.info('prepare for upgrade: %s', result)
    return result, state

async def upgrade(state):
    logger.info('start upgrade')
    state = await _log_update_packages(state, 'background package changes')
    await _save_timestamps(state.upgrade_timestamps, state.upgrade_timestamp_files)
    future = _start_upgrade_service(state)
    result = await _wait_service_done_print_log(future)
    if result == 'done':
        if not await _check_timestamps(state.upgrade_timestamps, state.upgrade_timestamp_files):
            result = 'not completed'
        else:
            state = await _log_update_packages(state, 'package changes after upgrade')
            if await _host_file_exists(state.reboot_required_file):
                result = 'reboot-required'
    logger.info('upgrade: %s', result)
    return result, state

async def reboot(state):
    logger.info('start reboot')
    systemd.reboot()
    return 'pending', state

async def after_reboot(state):
    logger.info('reboot completed')
    return 'done', state

async def _wait_service_done_print_log(future):
    job = await future
    logger.info('%s result: %s', job.service_name, job.result)
    try:
        journal = await systemd.get_journal_extract(job.service_name, job.start_time)
        logger.info('journal entry for %s\n%s', job.service_name, journal.rstrip())
    except systemd.RunError as e:
        logger.warning('could not read journal: %s', e)
    return job.result

async def _log_update_packages(state, info):
    packages = await _get_dpkg_currently_installed()
    if state.packages is not None and packages is not None:
        diff = _diff_packages(state.packages, packages)
        if len(diff) == 0:
            logger.info('no %s', info)
        else:
            logger.info('%s:\n%s', info, '\n'.join(diff))
            state.update_packages(packages)
    elif packages is not None:
        logger.info('init packages on topic')
        state.update_packages(packages)
    return state

def _verify_service(service_name):
    try:
        systemd.get_unit(service_name)
    except systemd.UnitStateError as e:
        logger.error('%s, cannot proceed', e)

async def _disable_unit(unit_name):
    try:
        unit = systemd.get_unit(unit_name)
        if unit.Unit.ActiveState == b'active':
            logger.info('%s is active, stopping it', unit_name)
            await systemd.sync_stop(unit_name)
        if unit.Unit.UnitFileState == b'enabled':
            logger.info('%s is enabled, disabling it', unit_name)
            ret = systemd.disable(unit_name)
            for (op, src, dest) in ret:
                logger.debug('%s %s %s', op.decode(), src.decode(), dest.decode())
    except systemd.UnitStateError:
        pass

async def _get_dpkg_currently_installed():
    cmd = [b'/usr/bin/dpkg-query', b'--show']
    try:
        pkgstr = await systemd.run_collect_output(cmd)
        pkgmap = {}
        for line in pkgstr.rstrip().split('\n'):
            pkg, vsn = line.split('\t')
            pkgmap[pkg] = vsn
        return pkgmap
    except systemd.RunError as e:
        logger.warning('could not collect currently installed packages: %s', e)
        return None

def _diff_packages(pkgs, newpkgs):
    diff = []
    for k in pkgs.keys() | newpkgs.keys():
        if k in pkgs.keys():
            if k not in newpkgs.keys():
                diff.append(f'deleted {k}:{pkgs[k]}')
            elif pkgs[k] != newpkgs[k]:
                diff.append(f'upgraded {k}: {pkgs[k]} -> {newpkgs[k]}')
        else:
            diff.append(f'installed new {k}:newpkgs[k]')
    return sorted(diff)

def _start_upgrade_service(state):
    logger.info("start %s", state.upgrade_service_name)
    ## service has type=oneshot
    ## we can just wait for the start job to complete, that means that
    ## the service has completed
    return systemd.init_start(state.upgrade_service_name)

def _start_prepare_service(state):
    logger.info("start %s", state.prepare_service_name)
    ## service has type=oneshot
    ## we can just wait for the start job to complete, that means that
    ## the service has completed
    return systemd.init_start(state.prepare_service_name)

async def _save_timestamps(timestamps, timestamp_files):
    for i, file in enumerate(timestamp_files):
        timestamps[i] = await _read_timestamp(file)

async def _check_timestamps(timestamps, timestamp_files):
    updated = True
    for i, file in enumerate(timestamp_files):
        new_timestamp = await _read_timestamp(file)
        if new_timestamp is None or new_timestamp == timestamps[i]:
            updated = False
        else:
            timestamps[i] = new_timestamp
    return updated

async def _host_file_exists(filepath):
    return bool(await _read_timestamp(filepath))

async def _read_timestamp(filepath):
    cmd = [b'/bin/stat', b'-c', b'%Y', filepath.encode()]
    try:
        return await systemd.run_collect_output(cmd)
    except systemd.RunError:
        ## file doesn't exist
        return None
