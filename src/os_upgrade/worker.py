import asyncio
import datetime
import enum
import os
import socket

import avassa_client
import websockets

from . import APPNAME, CONTROLLER_NAME, client, utils
from .client import RECONNECT_TIMEOUT

host = os.environ.get('HOST', 'localhost')
utils.setup_logging(host)

## asynchronous main loop
async def main(logger, operations):
    client_wrapper = await client.login(operations)
    state = WorkerState(await operations.init(host, client_wrapper))
    signal_handler = utils.SignalHandler(asyncio.get_running_loop())
    conditions = set()
    consumer_name = f'{APPNAME}-worker-{host}'
    unread = avassa_client.volga.Position.unread()
    while True:
        try:
            async with client_wrapper.get_control_topic_consumer(consumer_name, unread) as consumer:
                await consumer.more(1)
                logger.info("consuming from the volga topic")
                conditions = { asyncio.create_task(
                                   select_volga_msg(consumer, client_wrapper, operations)),
                               asyncio.create_task(
                                   signal_handler_wrapper(
                                       signal_handler, client_wrapper, operations)),
                               asyncio.create_task(
                                   await_refresh_token(client_wrapper, logger, None, 0))
                              }
                await worker_loop(conditions, state, logger)
        except websockets.exceptions.ConnectionClosed:
            logger.info("volga connection closed, wait %d seconds and reconnect", RECONNECT_TIMEOUT)
        except ConnectionRefusedError:
            logger.info("volga unreachable, wait %d seconds and retry", RECONNECT_TIMEOUT)
        except socket.gaierror:
            logger.info("volga unreachable, wait %d seconds and retry", RECONNECT_TIMEOUT)
        for cond in conditions:
            cond.cancel()
        conditions = set()
        await asyncio.sleep(RECONNECT_TIMEOUT)

class WorkerState:
    State = enum.Enum('State', ['IDLE', 'PREPARED', 'REBOOT_REQUIRED', 'EXIT'])
    def __init__(self, ext_state):
        self.state = self.State.IDLE
        self.data = ext_state

async def select_volga_msg(consumer, client_wrapper, operations):
    cancelled = False
    while not cancelled:
        try:
            msg = await consumer.recv(auto_more=1)
            if msg['producer-name'] == CONTROLLER_NAME: ## ignore messages from other workers
                payload = msg['payload']
                if payload['action'] == 'prepare' and host in payload['hosts']:
                    stop_time = datetime.datetime.fromisoformat(payload['not-after'])
                    if datetime.datetime.utcnow().replace(tzinfo=datetime.UTC) > stop_time:
                        async def abort_action(worker_state, _logger):
                            await consumer.ack()
                            worker_state.state = WorkerState.State.IDLE
                            return worker_state
                        return abort_action, select_volga_msg, \
                                (consumer, client_wrapper, operations)
                    async def prepare_action(worker_state, logger):
                        result, worker_state.data = await operations.prepare(worker_state.data)
                        message = {'action': 'prepare', 'result': result}
                        if result == 'done':
                            worker_state.state = WorkerState.State.PREPARED
                        else:
                            worker_state.state = WorkerState.State.IDLE
                        if client_wrapper.produce_once(client_wrapper.CONTROL_TOPIC, message):
                            await consumer.ack()
                        else:
                            logger.error(f"failed to produce prepare result {result}"\
                                          " on control topic")
                            worker_state.state = WorkerState.State.EXIT
                        return worker_state
                    return prepare_action, select_volga_msg, \
                            (consumer, client_wrapper, operations)
                if payload['action'] == 'upgrade' and payload['host'] == host:
                    async def upgrade_action(worker_state, logger):
                        if worker_state.state == WorkerState.State.PREPARED:
                            result, worker_state.data = await operations.upgrade(worker_state.data)
                            message = {'action': 'upgrade', 'result': result}
                            if result == 'reboot-required':
                                worker_state.state = WorkerState.State.REBOOT_REQUIRED
                            else:
                                worker_state.state = WorkerState.State.IDLE
                            if client_wrapper.produce_once(client_wrapper.CONTROL_TOPIC, message):
                                await consumer.ack()
                            else:
                                logger.error(f"failed to produce upgrade result {result}"\
                                              " on control topic")
                                worker_state.state = WorkerState.State.EXIT
                        else:
                            await consumer.ack()
                        return worker_state
                    return upgrade_action, select_volga_msg, (consumer, client_wrapper, operations)
                if payload['action'] == 'reboot' and payload['host'] == host:
                    async def reboot_action(worker_state, logger):
                        if worker_state.state == WorkerState.State.REBOOT_REQUIRED:
                            result, worker_state.data = await operations.reboot(worker_state.data)
                            if result == 'pending':
                                return worker_state
                        else:
                            result, worker_state.data = await operations.after_reboot(\
                                                                                worker_state.data)
                        ## if result != 'pending', i.e. failed to reboot
                        ## or worker_state.state != REBOOT_REQUIRED, i.e. state after the reboot
                        message = {'action': 'reboot', 'result': result}
                        if client_wrapper.produce_once(client_wrapper.CONTROL_TOPIC, message):
                            worker_state.state = WorkerState.State.IDLE
                            await consumer.ack()
                        else:
                            logger.error(f"failed to produce reboot result {result}"\
                                          " on control topic")
                            worker_state.state = WorkerState.State.EXIT
                        return worker_state
                    return reboot_action, select_volga_msg, (consumer, client_wrapper, operations)
        except asyncio.CancelledError:
            cancelled = True

async def signal_handler_wrapper(signal_handler, client_wrapper, operations):
    try:
        await signal_handler.future
        async def action(worker_state, _logger):
            result, worker_state.data = await operations.prepare(worker_state.data)
            if result != 'done':
                return worker_state
            result, worker_state.data = await operations.upgrade(worker_state.data)
            if result == 'reboot-required':
                _result, worker_state.data = operations.reboot(worker_state.data)
            return worker_state
        return action, signal_handler_wrapper, (signal_handler, client_wrapper, operations)
    except asyncio.CancelledError:
        signal_handler.on_cancelled()

async def await_refresh_token(client_wrapper, logger, timedelta, retries):
    async def noop(worker_state, _logger):
        return worker_state
    if timedelta is None:
        refresh_at = client_wrapper.sess.get_token_recommended_refresh()
        if refresh_at is None:
            return noop, None, None
        recommended_refresh = refresh_at - datetime.datetime.now(datetime.UTC)
        if recommended_refresh.total_seconds() > 0:
            return noop, await_refresh_token, (client_wrapper, logger, recommended_refresh, 3)
        if retries <= 0:
            async def out_of_retries(worker_state, logger):
                logger.error('token could not be refreshed, exiting')
                worker_state.state = WorkerState.State.EXIT
            return out_of_retries, None, None
        token_expiry = client_wrapper.sess.get_token_expiry()
        token_expiry_timedelta = token_expiry - datetime.datetime.now(datetime.UTC)
        wait_time = token_expiry_timedelta.total_seconds() / (retries+1)
        if wait_time > 0:
            timedelta = datetime.timedelta(seconds = wait_time)
            return noop, await_refresh_token, (client_wrapper, logger, timedelta, retries)
        async def expired(worker_state, logger):
            logger.error('token expired and could not be refreshed, exiting')
            worker_state.state = WorkerState.State.EXIT
        return expired, None, None
    total_seconds = timedelta.total_seconds()
    days = timedelta.days
    hours = timedelta.seconds // 3600
    minutes = (timedelta.seconds // 60) % 60
    seconds = timedelta.seconds % 60
    logger.info(f'will attempt token refresh {"in" if total_seconds > 0 else "now"}'
                f'{f" {days} days" if days > 0 else ""}'
                f'{f" {hours} hours" if hours > 0 else ""}'
                f'{f" {minutes} minutes" if minutes > 0 else ""}'
                f'{f" {seconds} seconds" if seconds > 0 else ""}')
    await asyncio.sleep(total_seconds)
    try:
        client_wrapper.sess.refresh_token()
        logger.info('token refresh successful')
    except avassa_client.LoginError as e:
        logger.error(f'failed to refresh token: {e}, retry')
        return noop, await_refresh_token, (client_wrapper, logger, None, retries-1)
    return noop, await_refresh_token, (client_wrapper, logger, None, 3)

async def worker_loop(conditions, state, logger):
    while state.state != WorkerState.State.EXIT:
        done, conditions = await asyncio.wait(conditions, return_when = asyncio.FIRST_COMPLETED)
        action, newcondition, params = done.pop().result()
        state = await action(state, logger)
        if newcondition is not None:
            conditions.add(asyncio.create_task(newcondition(*params)))
