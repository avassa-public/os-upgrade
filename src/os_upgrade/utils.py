import logging
import os
import signal
import sys

def setup_logging(host):
    logging.basicConfig(
            level = os.environ.get('LOG_LEVEL', 'INFO'),
            datefmt = '%Y-%m-%d %H:%M:%S',
            format = '<{levelname:5s}> {asctime}.{msecs:03.0f} %s {name}\n{message}' % host,
            style = '{'
            )
    sys.excepthook = logging.exception

class SignalHandler:
    def __init__(self, loop):
        self.loop = loop
        self.future = self.loop.create_future()
        loop.add_signal_handler(signal.SIGUSR1, self.sigusr1)

    def sigusr1(self):
        newfuture = self.loop.create_future()
        self.future.set_result(None)
        self.future = newfuture

    def on_cancelled(self):
        self.future = self.loop.create_future()
