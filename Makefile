VSN = $(shell grep ^version pyproject.toml | cut -d= -f2 | tr -d ' "')

build-dev:
	docker build --tag avassa/os-upgrade \
				 --build-arg VSN=$(VSN) \
				 -f Dockerfile.build .
.PHONY: build-dev

requirements.txt:
	rm -f $@
	pip-compile -q --strip-extras -o $@
	chmod -w $@
.PHONY: requirements.txt

check: src
	pylint $<
	find $< -name __main__.py | xargs pyright
.PHONY: check
